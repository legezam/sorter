﻿using Sorter.API.Heap;
using Sorter.API.Support;

namespace Sorter.Algorithms
{
  /// <summary>
  /// Buborékrendezést megvalósító osztály.
  /// </summary>
  internal class BubbleSort : InplaceSortBase<int>
  {
    
    public BubbleSort(IHeap heap): base(heap)
    {
    }

    public override string Name => "Bubble";

    protected override int[] SortInplace(int[] source)
    {
      using (this.heap.Allocate(1))
        for (int i = source.Length - 1; i >= 0; i--)
        {
          using (heap.Allocate(1))
            for (int j = 0; j < i; j++)
            {
              if (source[j] > source[j + 1])
              {
                Swap(source, j, j + 1);
              }
              this.perfCounter.Comparisons++;
              this.perfCounter.Iterations++;
            }
          RaiseTurnCompleted(source, source.Length - i);
        }

      return source;
    }
  }
}
