﻿using Sorter.API.Heap;
using Sorter.API.Support;

namespace Sorter.Algorithms
{
  /// <summary>
  /// Javított buborékrendezést megvalósító osztály.
  /// </summary>
  internal class EnhancedBubbleSort : InplaceSortBase<int>
  {
    public EnhancedBubbleSort(IHeap heap) : base(heap)
    {
    }

    public override string Name => "EnhancedBubble";

    protected override int[] SortInplace(int[] source)
    {
      using (this.heap.Allocate(1))
      {
        int i = source.Length - 1;

        while (i >= 1)
          using (heap.Allocate(1))
          {
            int CS = 0;

            using(heap.Allocate(1))
            for (int j = 0; j < i; j++)
            {
              if (source[j] > source[j + 1])
              {
                Swap(source, j, j + 1);
                CS = j;
              }
              this.perfCounter.Comparisons++;
              this.perfCounter.Iterations++;
            }
            i = CS;
            RaiseTurnCompleted(source, source.Length - i);
          }
        return source;
      }
    }
  }
}
