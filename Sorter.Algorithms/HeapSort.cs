﻿using Sorter.API.Heap;
using Sorter.API.Support;
namespace Sorter.Algorithms
{
  /// <summary>
  /// Kupacrendezést megvalósító osztály.
  /// </summary>
  internal class HeapSort : InplaceSortBase<int>
  {
    public override string Name => "Heap";

    public HeapSort(IHeap heap) : base(heap)
    {
    }

    protected override int[] SortInplace(int[] source)
    {
      return this.KupacRendezes(source, source.Length);
    }

    private int[] KupacRendezes(int[] tomb, int tombMeret)
    {
      this.perfCounter.MethodCalls++;

      KupacotEpit(tomb, tombMeret);
      RaiseTurnCompleted(tomb);
      using (heap.Allocate(1))
        for (int i = tombMeret - 1; i >= 1; i--)
        {
          Swap(tomb, 0, i);
          tombMeret = tombMeret - 1;
          Kupacol(tomb, 0, tombMeret);
        }
      return tomb;
    }

    private void KupacotEpit(int[] tomb, int tombMeret)
    {
      this.perfCounter.MethodCalls++;
      using (heap.Allocate(1))
        for (int i = tombMeret / 2; i >= 0; i--)
        {
          Kupacol(tomb, i, tombMeret);
          this.perfCounter.Iterations++;
        }

    }

    private void Kupacol(int[] tomb, int i, int tombMeret)
    {
      using (heap.Allocate(3))
      {
        this.perfCounter.MethodCalls++;

        int bal = GetBalIndex(i);
        int jobb = GetJobbIndex(i);
        int MAX = 0;
        if (bal < tombMeret && tomb[bal] > tomb[i])
        {
          MAX = bal;
        }
        else
        {
          MAX = i;
        }

        if (jobb < tombMeret && tomb[jobb] > tomb[MAX])
        {
          MAX = jobb;
        }

        if (MAX != i)
        {
          Swap(tomb, i, MAX);
          RaiseTurnCompleted(tomb);
          Kupacol(tomb, MAX, tombMeret);
        }
        this.perfCounter.Comparisons += 3;
      }
    }

    private static int GetSzuloIndex(int i)
    {
      return i / 2;
    }

    private static int GetBalIndex(int i)
    {
      return (i == 0) ? 1 : 2 * i;
    }

    private static int GetJobbIndex(int i)
    {
      return (i == 0) ? 2 : 2 * i + 1;
    }
  }
}
