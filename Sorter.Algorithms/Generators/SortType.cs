﻿namespace Sorter.Algorithms.Generators
{
  /// <summary>
  /// Előre elkészített rendezőalgoritmusok felsorolása.
  /// </summary>
  public enum SortType
  {
    Buborek,
    JavitottBuborek,
    HeapSort,
    QuickSort,
    MergeSort,
    ShellSort,
    Beilleszteses,
    JavitottBeilleszteses,
    Introspection
  }
}