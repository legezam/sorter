﻿using Sorter.API.Heap;
using Sorter.API.Support;
using System;

namespace Sorter.Algorithms.Generators
{
  /// <summary>
  /// Rendezoalgoritmusokat inicianilzalo fuggvenyeket keszito osztaly.
  /// </summary>
  public class SortGenerator : ISortGenerator<int>
  {
    /// <summary>
    /// Singleton peldany adattagja.
    /// </summary>
    private static readonly SortGenerator defaultInstance = new SortGenerator();

    /// <summary>
    /// Singleton peldanyt visszaado Property.
    /// </summary>
    public static ISortGenerator<int> Default => defaultInstance;

    /// <summary>
    /// Legyart egy a SortTypenak megfelelo rendezesi algoritmust keszito fuggvenyt.
    /// </summary>
    public virtual Func<IHeap, ISort<int>> GetGenerator(SortType sortType)
    {
      switch (sortType)
      {
        case SortType.Buborek:
          return (heap) => new BubbleSort(heap);
        case SortType.HeapSort:
          return (heap) => new HeapSort(heap);
        case SortType.QuickSort:
          return (heap) => new QuickSort(heap);
        case SortType.MergeSort:
          return (heap) => new MergeSort(heap);
        case SortType.ShellSort:
          return (heap) => new ShellSort(heap);
        case SortType.JavitottBuborek:
          return (heap) => new EnhancedBubbleSort(heap);
        case SortType.Beilleszteses:
          return (heap) => new InsertionSort(heap);
        case SortType.Introspection:
          return (heap) => new IntrospectiveSort(heap);
        case SortType.JavitottBeilleszteses:
          return (heap) => new EnhancedInsertionSort(heap);
        default:
          throw new ArgumentOutOfRangeException("sortType");
      }
    }
  }
}
