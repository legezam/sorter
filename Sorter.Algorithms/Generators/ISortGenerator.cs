﻿using Sorter.API.Heap;
using Sorter.API.Support;
using System;

namespace Sorter.Algorithms.Generators
{
  /// <summary>
  /// Publikus interfesz, mely eloirja, hogy egy Rendezoalgoritmus generatornak milyen tagjai vannak.
  /// </summary>
  /// <typeparam name="T">Barmilyen IComparable interfeszt megvalosito tipus</typeparam>
  public interface ISortGenerator<T>
    where T : IComparable<T>
  {
    /// <summary>
    /// A Legyart egy SortTypenak megfelelo rendezesi algoritmust keszito fuggvenyt.
    /// </summary>
    /// <param name="sortType">Rendezoalgoritmus tipusa.</param>
    /// <returns>Rendezoalgoritmust gyartani kepes fuggveny.</returns>
    Func<IHeap, ISort<T>> GetGenerator(SortType sortType);
  }
}
