﻿using Sorter.API;
using Sorter.API.Heap;
using Sorter.API.Support;

namespace Sorter.Algorithms
{
  /// <summary>
  /// Javított beillesztéses rendezést megvalósító osztály.
  /// </summary>
  internal class EnhancedInsertionSort : InplaceSortBase<int>
  {

    public EnhancedInsertionSort(IHeap heap) : base(heap)
    {
    }

    public override string Name => "EnhancedInsertion";

    protected override int[] SortInplace(int[] source)
    {
      using (this.heap.Allocate(1))      
        for (int i = 1; i < source.Length; i++)
          using (heap.Allocate(2))
          {
            int j = i - 1;
            int Y = source[i];

            bool innerLoopExecuted = false;
            while (j >= 0 && source[j] > Y)
            {
              source[j + 1] = source[j];
              
              j = j - 1;

              this.perfCounter.Comparisons++;
              this.perfCounter.Iterations++;
              innerLoopExecuted = true;
            }

            source[j + 1] = Y;
            this.RaiseTurnCompleted(source, i);

            this.perfCounter.Comparisons = this.perfCounter.Comparisons.IncrementIf(!innerLoopExecuted);
            this.perfCounter.Iterations = this.perfCounter.Iterations.IncrementIf(!innerLoopExecuted);
          }

        return source;
      }
    
  }
}
