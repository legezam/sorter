﻿using Sorter.API.Heap;
using Sorter.API.Support;
using System;
using System.Collections.Generic;

namespace Sorter.Algorithms
{
  /// <summary>
  /// MergeSort rendezőalgoritmust megvalósító osztály.
  /// </summary>
  internal class MergeSort : InplaceSortBase<int>
  {
    public override string Name => "Merge";

    public MergeSort(IHeap heap): base(heap)
    {
    }

    protected override int[] SortInplace(int[] source)
    {
      return MergeRendezes(source, 0, source.Length - 1);
    }
    public int[] MergeRendezes(int[] tomb, int elso, int utolso)
    {
      this.perfCounter.MethodCalls++;
      if (elso < utolso)
      {
        using (this.heap.Allocate(1))
        {
          int kozepso = (elso + utolso) / 2;
          MergeRendezes(tomb, elso, kozepso);
          MergeRendezes(tomb, kozepso + 1, utolso);
          Merge(tomb, elso, ref kozepso, utolso);
        }
      }
      return tomb;
    }

    private void Merge(int[] tomb, int elso, ref int kozepso, int utolso)
    {
      this.perfCounter.MethodCalls++;
      using (this.heap.Allocate((utolso - elso + 7))) //bal list + jobb list + 2 (i,j) + 3 (k,l, m) + 2 (also,felso hossz)
      {
        int alsoHossz = kozepso - elso + 1;
        int felsoHossz = utolso - kozepso;

        List<int> balList = new List<int>();
        List<int> jobbList = new List<int>();
        for (int i = 1; i <= alsoHossz; i++)
        {
          this.perfCounter.Iterations++;
          balList.Add(tomb[elso + i - 1]);
        }
        for (int j = 1; j <= felsoHossz; j++)
        {
          this.perfCounter.Iterations++;
          jobbList.Add(tomb[kozepso + j]);
        }
        balList.Add(Int32.MaxValue);
        jobbList.Add(Int32.MaxValue);
        int k = 0;
        int l = 0;

        for (int m = elso; m <= utolso; m++)
        {
          this.perfCounter.Iterations++;
          if (balList[k] <= jobbList[l])
          {
            tomb[m] = balList[k];
            k = k + 1;
          }
          else
          {
            tomb[m] = jobbList[l];
            l = l + 1;
          }
          this.perfCounter.Comparisons++;
        }
      }
      this.RaiseTurnCompleted(tomb);
      
    }
  }
}
