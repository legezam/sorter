﻿using Sorter.API;
using Sorter.API.Heap;
using Sorter.API.Support;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sorter.Algorithms
{
  /// <summary>
  /// Beillesztéses rendezést megvalósító osztály.
  /// </summary>
  internal class InsertionSort : InplaceSortBase<int>
  {

    public InsertionSort(IHeap heap) : base(heap)
    {
    }

    public override string Name => "Insertion";

    protected override int[] SortInplace(int[] source)
    {
      using (this.heap.Allocate(1))
        for (int i = 1; i < source.Length; i++)
          using (heap.Allocate(1))
          {
            int j = i - 1;
            bool innerLoopExecuted = false;
            while (j >= 0 && source[j] > source[j + 1])
            {
              Swap(source, j, j + 1);
              j = j - 1;
              this.perfCounter.Comparisons++;
              this.perfCounter.Iterations++;
              innerLoopExecuted = true;
            }
            this.RaiseTurnCompleted(source, i);
            this.perfCounter.Comparisons = this.perfCounter.Comparisons.IncrementIf(!innerLoopExecuted);
            this.perfCounter.Iterations = this.perfCounter.Iterations.IncrementIf(!innerLoopExecuted);
          }

      return source;
    }
  }
}
