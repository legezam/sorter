﻿using Sorter.API.Heap;
using Sorter.API.Support;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sorter.Algorithms
{
  /// <summary>
  /// Introspective Rendező algoritmust megvalósító osztály.
  /// </summary>
  /// <remarks>A kód eredeti verziója a https://www.programmingalgorithms.com/algorithm/intro-sort?lang=C%23 webhelyről származik.</remarks>
  internal class IntrospectiveSort : InplaceSortBase<int>
  {
    public override string Name => "Introspective";

    public IntrospectiveSort(IHeap heap) : base(heap) { }

    protected override int[] SortInplace(int[] source)
    {
      IntroSort(ref source);
      RaiseTurnCompleted(source);
      return source;
    }

    private void IntroSort(ref int[] data)
    {
      RaiseTurnCompleted(data);

      int partitionSize = Partition(ref data, 0, data.Length - 1);

      if (partitionSize < 16)
      {
        InsertionSort(ref data);
      }
      else if (partitionSize > (2 * Math.Log(data.Length)))
      {
        HeapSort(ref data);
      }
      else
      {
        QuickSortRecursive(ref data, 0, data.Length - 1);
      }
    }

    private void InsertionSort(ref int[] data)
    {
      this.perfCounter.MethodCalls++;

      using (heap.Allocate(1))
        for (int i = 1; i < data.Length; ++i)
          using (heap.Allocate(1))
          {
            int j = i;

            while ((j > 0))
            {
              if (data[j - 1] > data[j])
              {
                data[j - 1] ^= data[j];
                data[j] ^= data[j - 1];
                data[j - 1] ^= data[j];

                --j;
                RaiseTurnCompleted(data);
              }
              else
              {
                break;
              }
              this.perfCounter.Comparisons++;
              this.perfCounter.Iterations++;
            }
          }
    }


    private void HeapSort(ref int[] data)
    {
      this.perfCounter.MethodCalls++;

      using (heap.Allocate(1))
      {
        int heapSize = data.Length;

        using (heap.Allocate(1))
          for (int p = (heapSize - 1) / 2; p >= 0; --p)
          {
            MaxHeapify(ref data, heapSize, p);
            this.perfCounter.Iterations++;
          }

        using (heap.Allocate(1))
          for (int i = data.Length - 1; i > 0; --i)
          {
            Swap(data, i, 0);

            --heapSize;
            MaxHeapify(ref data, heapSize, 0);
            this.perfCounter.Iterations++;
          }
      }
    }

    private void MaxHeapify(ref int[] data, int heapSize, int index)
    {
      this.perfCounter.MethodCalls++;
      using (heap.Allocate(3))
      {
        int left = (index + 1) * 2 - 1;
        int right = (index + 1) * 2;
        int largest = 0;

        if (left < heapSize && data[left] > data[index])
          largest = left;
        else
          largest = index;

        if (right < heapSize && data[right] > data[largest])
          largest = right;

        if (largest != index)
        {
          Swap(data, index, largest);
          RaiseTurnCompleted(data);
          MaxHeapify(ref data, heapSize, largest);
        }

        this.perfCounter.Comparisons += 2;
      }
    }

    private void QuickSortRecursive(ref int[] data, int left, int right)
    {
      this.perfCounter.MethodCalls++;
      if (left < right)
      {
        using (heap.Allocate(1))
        {
          int q = Partition(ref data, left, right);
          QuickSortRecursive(ref data, left, q - 1);
          QuickSortRecursive(ref data, q + 1, right);
        }
      }
    }

    private int Partition(ref int[] data, int left, int right)
    {
      this.perfCounter.MethodCalls++;
      using (heap.Allocate(2))
      {
        int pivot = data[right];
        int i = left;

        using (heap.Allocate(1))
          for (int j = left; j < right; ++j)
          {
            if (data[j] <= pivot)
            {
              Swap(data, i, j);
              i++;
            }
            this.perfCounter.Comparisons++;
            this.perfCounter.Iterations++;
          }

        data[right] = data[i];
        data[i] = pivot;

        RaiseTurnCompleted(data);
        return i;
      }
    }
  }
}
