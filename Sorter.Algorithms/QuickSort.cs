﻿using Sorter.API.Heap;
using Sorter.API.Support;

namespace Sorter.Algorithms
{
  /// <summary>
  /// Gyorsrendezést megvalósító osztály.
  /// </summary>
  internal class QuickSort : InplaceSortBase<int>
  {
    public override string Name => "QuickSort";

    public QuickSort(IHeap heap):base(heap)
    {
    }

    protected override int[] SortInplace(int[] source)
    {
            RaiseTurnCompleted(source);
      QuickRendezes(source, 0, source.Length - 1);
      return source;
    }

    public int[] QuickRendezes(int[] tomb, int elso, int utolso)
    {
      using (heap.Allocate(1))
      {
        this.perfCounter.MethodCalls++;
        int kozepso = (elso + utolso) / 2;

        Szetvalogat(tomb, elso, utolso, ref kozepso);
        if (kozepso - elso > 1)
        {
          QuickRendezes(tomb, elso, kozepso - 1);
        }
        if (utolso - kozepso > 1)
        {
          QuickRendezes(tomb, kozepso + 1, utolso);
        }
        return tomb;
      }
    }

    private void Szetvalogat(int[] tomb, int elso, int utolso, ref int kozepso)
    {
      using (heap.Allocate(2))
      {
        this.perfCounter.MethodCalls++;
        kozepso = elso;
        int jobb = utolso;
        int A = tomb[kozepso];

        while (kozepso < jobb)
        {
          while (kozepso < jobb && tomb[jobb] >= A)
          {
            jobb = jobb - 1;
            this.perfCounter.Iterations++;
            this.perfCounter.Comparisons++;
          }
          if (kozepso < jobb)
          {
            tomb[kozepso] = tomb[jobb];
            kozepso = kozepso + 1;
            while (kozepso < jobb && tomb[kozepso] <= A)
            {
              kozepso = kozepso + 1;
              this.perfCounter.Iterations++;
              this.perfCounter.Comparisons++;
            }
            if (kozepso < jobb)
            {
              tomb[jobb] = tomb[kozepso];
              jobb = jobb - 1;
            }
          }
        }
        tomb[kozepso] = A;
        RaiseTurnCompleted(tomb);
      }
    }
  }
}
