﻿using Sorter.API;
using Sorter.API.Heap;
using Sorter.API.Support;

namespace Sorter.Algorithms
{
  /// <summary>
  /// Shellrendezést megvalósító osztály.
  /// </summary>
  internal class ShellSort : InplaceSortBase<int>
  {
    public override string Name => "Shell";

    public ShellSort(IHeap heap) : base(heap)
    {
    }

    protected override int[] SortInplace(int[] source)
    {
      return this.ShellRendezes(source, source.Length);
    }

    public int[] ShellRendezes(int[] tomb, int tombMeret)
    {
      using (heap.Allocate(1))
      {
        int ablakMeret = tombMeret / 2;

        RaiseTurnCompleted(tomb);
        while (ablakMeret > 0)
        {
          using (heap.Allocate(1))
            for (int k = ablakMeret; k <= 2 * ablakMeret; k++)
            {
              using (heap.Allocate(1))
                for (int i = k; i < tombMeret; i = i + ablakMeret)
                {
                  using (heap.Allocate(2))
                  {
                    int j = i - ablakMeret;
                    int Y = tomb[i];

                    bool innerLoopExecuted = false;
                    while (j >= 0 && tomb[j] > Y)
                    {
                      Swap(tomb, j + ablakMeret, j);
                      j = j - ablakMeret;
                      this.perfCounter.Iterations++;
                      this.perfCounter.Comparisons++;
                      innerLoopExecuted = true;
                    }
                    tomb[j + ablakMeret] = Y;
                    this.perfCounter.Iterations = this.perfCounter.Iterations.IncrementIf(!innerLoopExecuted);
                    this.perfCounter.Comparisons = this.perfCounter.Comparisons.IncrementIf(!innerLoopExecuted);
                  }
                }
              RaiseTurnCompleted(tomb);
            }
          ablakMeret = ablakMeret / 2;
          RaiseTurnCompleted(tomb);
        }
      }
      return tomb;
    }
  }
}
