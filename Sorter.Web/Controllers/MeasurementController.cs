﻿using Newtonsoft.Json;
using Sorter.Algorithms.Generators;
using Sorter.API.Measurement;
using Sorter.API.SequenceGenerators;
using Sorter.Web.Models;
using System;
using System.IO;
using System.Web.Mvc;

namespace Sorter.Web.Controllers
{
  public class MeasurementController : Controller
  {
    private const string ResultsFolder = "~/Content/Results";
    public MeasurementController()
    {

    }

    public ActionResult Index()
    {
      if (!Directory.Exists(Server.MapPath(ResultsFolder)))
      {
        Directory.CreateDirectory(Server.MapPath(ResultsFolder));
      }

      return View();
    }
    
    [HttpGet]
    public ActionResult Result(string sequenceType, int sequenceSize, MeasurementType measurementType, SortType sortType)
    {
      try
      {
        Guid sessionGuid = Guid.NewGuid();
        MeasurementSessionResult result = null;
        using (var session = new MeasurementSession<int>(
          new MeasurementSetup<int>.Builder()
          {
            Factory = SortGenerator.Default.GetGenerator(sortType),
            Measurement = measurementType,
            SequenceSize = sequenceSize,
            SequenceType = SequenceProvider<int>.ParseIntegerGenerator(sequenceType),
            SessionTime = DateTime.Now,
            OutputPath = Server.MapPath(ResultsFolder)
          }))
        {
          result = session.StartSession();
        }


        MeasurementResultViewModel vm = new MeasurementResultViewModel()
        {
          Result = result,
          MeasurementType = measurementType,
          SequenceSize = sequenceSize,
          SequenceType = sequenceType,
          SortType = sortType,
          SessionId = sessionGuid
        };

        System.IO.File.WriteAllText(Path.Combine(Server.MapPath(ResultsFolder), $"{sessionGuid.ToString()}_Result.txt"), JsonConvert.SerializeObject(vm, Formatting.Indented));
        return View(vm);
      }
      catch (Exception)
      {
        return View("Error", (object)"Hiba történt a művelet során.");
      }
    }

    [HttpGet]
    public ActionResult SavedSession(Guid sessionId)
    {

      try
      {
        MeasurementResultViewModel vm = JsonConvert.DeserializeObject<MeasurementResultViewModel>(
            System.IO.File.ReadAllText(
              Path.Combine(Server.MapPath(ResultsFolder), $"{sessionId.ToString()}_Result.txt")));
        return View("Result", vm);
      }
      catch (Exception)
      {
        return View("Error", (object)"Hiba történt a művelet során.");
      }
    }

    [HttpGet]
    [HandleError]
    public ViewResult NotFound()
    {
      Response.StatusCode = 404;
      return View("Error");
    }
  }
}