//TurnCompletedEventArgs TypeScript-es megfeleloje
var State = (function () {
    function State() {
    }
    return State;
})();
//DeallocationEventArgs TypeScript-es megfeleloje
var Dealloc = (function () {
    function Dealloc() {
    }
    return Dealloc;
})();
// Lepesanalizis, Tavolsaganalizis, TavolsagDiagram kezdeti defininialasa:
// Mindharom ugyanakkora diagramteruletre kerul kirajzolasra
var margin = { top: 20, right: 20, bottom: 30, left: 40 }, width = 900 - margin.left - margin.right, height = 600 - margin.top - margin.bottom;
//Lepesanalizis X skalaja
var stepXscale = d3.scale.linear()
    .range([0, width]);
//Lepesanalizis Y skalaja
var stepYscale = d3.scale.linear()
    .range([0, height]);
//Lepesanalizis X tengelye
var stepXaxis = d3.svg.axis()
    .scale(stepXscale)
    .innerTickSize(-height)
    .orient("bottom");
//Lepesanalizis Y tengelye
var stepYaxis = d3.svg.axis()
    .scale(stepYscale)
    .innerTickSize(-width)
    .orient("left");
//Lepesanalizis rajzterulete
var stepSvg = d3.select(".lepesanalizis")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom);
//Tavolsaganalizis X skalaja
var distanceXscale = d3.scale.linear()
    .range([0, width]);
//Tavolsaganalizis Y skalaja
var distanceYscale = d3.scale.linear()
    .range([0, height]);
//Tavolsaganalizis X tengelye
var distanceXaxis = d3.svg.axis()
    .scale(distanceXscale)
    .innerTickSize(-height)
    .orient("bottom");
//Tavolsaganalizis Y tengelye
var distanceYaxis = d3.svg.axis()
    .scale(distanceYscale)
    .orient("left");
//Tavolsaganalizis rajzterulete
var distanceSvg = d3.select(".tavolsaganalizis")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom);
//TavolsagDiagram X skalaja
var distanceDiagXscale = d3.scale.linear()
    .range([0, width]);
//TavolsagDiagram Y skalaja
var distanceDiagYScale = d3.scale.linear()
    .range([0, height]);
//TavolsagDiagram X tengelye
var distanceDiagXaxis = d3.svg.axis()
    .scale(distanceDiagXscale)
    .innerTickSize(-height)
    .orient("bottom");
//TavolsagDiagram Y tengelye
var distanceDiagYaxis = d3.svg.axis()
    .scale(distanceDiagYScale)
    .innerTickSize(-width)
    .orient("left");
//TavolsagDiagram rajzterulete
var distanceDiagSvg = d3.select(".tavolsagdiagram")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom);
var turnCompletedPath; //Kis csalas, Razor engine kitolti a szerver oldalon
var deallocationPath;
// Lepesanalizis, Tavolsaganalizis, TavolsagDiagram kezdeti defininialasa vege
//Lepesanalizis, Tavolsaganalizis, TavolsagDiagram bemeno adatok lekerese a szervertol es a diagram kirajzolasa
d3.json(turnCompletedPath, function (error, data) {
    //Rendezesek elso kore, kezdeti allapot -> [elem indexe, elem erteke]
    var firstIteration = data[0].state.map(function (v, i, a) { return [i, v]; });
    //Tavolsagdiagram skalajanak ertelmezesi tartomanya
    distanceDiagXscale.domain([0, data.length]);
    distanceDiagYScale.domain([data[0].state.length, 0]);
    //Tavolsagdiagram Y tengelyenek kirajzolasa
    distanceDiagSvg.append("g")
        .attr("class", "y axis")
        .attr("transform", "translate(" + margin.left + ", " + margin.top + ")")
        .call(distanceDiagYaxis);
    //TavolsagDiagram X tengelyenek kriajzolasa
    distanceDiagSvg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(" + margin.left + ", " + (height + margin.top) + ")")
        .call(distanceDiagXaxis);
    //TavolsagDiagram belso (pontokat tartalmazo) rajzteruletenek kirajzolasa
    var distanceDiagContent = distanceDiagSvg
        .append("g")
        .attr("transform", "translate(" + margin.left + ", " + margin.top + ")");
    //TavolsagDiagram: Altagos abszolut tavolsag a vegleges helytol adatsor kirajzolasa
    distanceDiagContent.selectAll(".circleMean")
        .data(data) //Adatkotes
        .enter()
        .append("circle")
        .attr("class", ".circleMean")
        .attr("r", 2)
        .attr("stroke", "yellowgreen")
        .attr("fill", "yellowgreen")
        .attr("opacity", 0.7)
        .attr("cx", function (d, i, o) { return distanceDiagXscale(i); }) //Kor indexe
        .attr("cy", function (d, i, o) { return distanceDiagYScale(d3.mean(d.state.map(function (v, i, a) { return Math.abs(i - v); }))); }); //Tavolsagok atlaga
    //TavolsagDiagram: Maximum tavolsag a vegleges helytol adatsor kirajzolasa
    distanceDiagContent.selectAll(".circleMax")
        .data(data) //Adatkotes
        .enter()
        .append("circle")
        .attr("class", ".circleMax")
        .attr("r", 2)
        .attr("stroke", "orange")
        .attr("fill", "orange")
        .attr("opacity", 0.7)
        .attr("cx", function (d, i, o) { return distanceDiagXscale(i); }) //Kor indexe
        .attr("cy", function (d, i, o) { return distanceDiagYScale(d3.max(d.state.map(function (v, i, a) { return Math.abs(i - v); }))); }); //Tavolsagok maximuma
    //TavolsagDiagram: Helyen levo elemek szama adatsor kirajzolasa
    distanceDiagContent.selectAll(".circleInPlace")
        .data(data) //Adatkotes
        .enter()
        .append("circle")
        .attr("class", ".circleInPlace")
        .attr("r", 2)
        .attr("stroke", "cornflowerblue")
        .attr("fill", "cornflowerblue")
        .attr("opacity", 0.7)
        .attr("cx", function (d, i, o) { return distanceDiagXscale(i); }) //Kor indexe
        .attr("cy", function (d, i, o) { return distanceDiagYScale(d3.sum(d.state.map(function (v, i, a) { return i - v == 0 ? 1 : 0; }))); }); //Helyen levo elemek szama
    //Lepes analizis setup
    //Lepesanalizis skalak ertelmezesi tartomanyanak megadasa
    stepXscale.domain([0, data[0].state.length]);
    stepYscale.domain([data[0].state.length, 0]);
    //Lepesanalizis Y tengelyenek kirajzolasa
    stepSvg.append("g")
        .attr("class", "y axis")
        .attr("transform", "translate(" + margin.left + ", " + margin.top + ")")
        .call(stepYaxis);
    //Lepesanalizis X tengelyenek kirajzolasa
    stepSvg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(" + margin.left + ", " + (height + margin.top) + ")")
        .call(stepXaxis);
    //Lepesanalizis belso (pontokat tartalmazo) rajzteruletenek kirajzolasa
    var stepContent = stepSvg.append("g")
        .attr("transform", "translate(" + margin.left + ", " + margin.top + ")");
    //Lepesanalizis tomb kezdeti allapotat tukrozo pontsoranak kirajzolasa
    stepContent.selectAll("circle")
        .data(firstIteration, function (d) { return d[1]; }) //Adatkotes
        .enter().append("circle")
        .attr("r", 3)
        .attr("stroke", "black")
        .attr("fill", "cornflowerblue")
        .attr("cx", function (d) { return stepXscale(d[0]); }) //Elem indexe
        .attr("cy", function (d) { return stepYscale(d[1]); }); //Elem erteke
    //Csuszka kirajzolasa, esemenykezelo hozzarendelese
    var stepSlider = d3.select(".lepteto")
        .attr("type", "range")
        .attr("class", "slider")
        .attr("min", 0)
        .attr("max", data.length)
        .attr("value", 0)
        .on("input", function (d, i, o) {
        var idx = stepSlider.property("value");
        var dat = data[idx];
        var res = dat.state.map(function (n, i, a) { return [i, n]; });
        stepContent.selectAll("circle")
            .data(res, function (d) { return d[1]; }) //Uj adatkotes, a csuszkanak megfeleleo kor atatsoranak hozzarendelese az elemekhez
            .attr("r", 3)
            .transition()
            .attr("cx", function (d) { return stepXscale(d[0]); }) //Elem indexe
            .attr("cy", function (d) { return stepYscale(d[1]); }); //Elem erteke
    });
    //Tavolsag analizis setup
    //Tavolsaganalizis skalaja ertelmezesi tartomany definicioja
    distanceXscale.domain([-1 * data[0].state.length, data[0].state.length]);
    distanceYscale.domain([data[0].state.length, 0]);
    //Tavolsaganalizis Y tengelyenek kirajzolasa
    distanceSvg.append("g")
        .attr("class", "y axis")
        .attr("transform", "translate(" + (margin.left + width / 2) + ", " + margin.top + ")")
        .call(distanceYaxis);
    //Tavolsaganalizis X tengelyenek kirajzolasa
    distanceSvg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(" + margin.left + ", " + (height + margin.top) + ")")
        .call(distanceXaxis);
    //Tavolsaganalizis belso (pontokat tartalmazo) rajzteruletenek kirajzolasa
    var distanceContent = distanceSvg.append("g")
        .attr("transform", "translate(" + margin.left + ", " + margin.top + ")");
    //Tavolsaganalizis kezdeti allapotot jelkepezo potsoranak kirajzolasa
    distanceContent.selectAll("circle")
        .data(firstIteration, function (d) { return d[1]; }) //Adatkotes
        .enter().append("circle")
        .attr("r", 3)
        .attr("stroke", "black")
        .attr("fill", "cornflowerblue")
        .attr("cx", function (d) { return distanceXscale(d[0] - d[1]); }) //Elem indexebol kivonva az elem erteke
        .attr("cy", function (d) { return distanceYscale(d[1]); }); //Elem erteke
    //Csuszka kirajzolasa, esemenykezelo hozzarendelese
    var distanceSlider = d3.select(".tavolsaglepteto")
        .attr("type", "range")
        .attr("class", "slider")
        .attr("min", 0)
        .attr("max", data.length)
        .attr("value", 0)
        .on("input", function (d, i, o) {
        var idx = distanceSlider.property("value");
        var dat = data[idx];
        var res = dat.state.map(function (n, i, a) { return [i, n]; });
        distanceContent.selectAll("circle")
            .data(res, function (d) { return d[1]; }) //Uj adatkotes, a csuszkanak megfeleleo kor atatsoranak hozzarendelese az elemekhez
            .transition()
            .attr("cx", function (d) { return distanceXscale(d[0] - d[1]); }); //Elem indexebol kivonva az elem erteke
    });
});
// Memoriaterkep kezdeti defininialasa
// A memoriaterkep rajzteruletenek meret definialasa
var memoryMapWidth = 900 - margin.left - margin.right, memoryMapHeight = 600 - margin.top - margin.bottom;
//Memoriaterkep rajzterulet kirajzolasa
var memoryMapSvg = d3.select(".memoriaterkep")
    .attr("width", memoryMapWidth + margin.left + margin.right)
    .attr("height", memoryMapHeight + margin.top + margin.bottom);
//Zoom faktor kezdeti erteke
var zoomThreshold = 1.0;
//D3.Js altal nyujtott szinezes
var randomColors = d3.scale.category20();
//Memoriaterkep kezdeti defininialasa vege
//Memoriaterkep bemeno adatok lekerese a szervertol es a diagram kirajzolasa
d3.json(deallocationPath, function (error, data) {
    //Letutolso deallokacio idopontja (algoritmus futasanak vege)
    var maxAllocationTime = d3.max(data, function (d) { return d.deallocTime; });
    //Memoriaterkep virtualis rajzteruletenek meretbeallitasa az adatsor hosszanak fuggvenyeben (a tobb ideig tarto rendezeseket "szet kell huzni")
    var memoryMapWidth = (maxAllocationTime / 25) - margin.left - margin.right, memoryMapHeight = 600 - margin.top - margin.bottom;
    memoryMapSvg.attr("width", memoryMapWidth + margin.left + margin.right);
    //Memoriaterkep X skalajanak beallitasa
    var memoryMapXscale = d3.scale.linear()
        .range([0, memoryMapWidth])
        .domain([0, maxAllocationTime]);
    //Memoriaterkep Y skalajanak beallitasa
    var memoryMapYscale = d3.scale.linear()
        .range([0, memoryMapHeight])
        .domain([0, d3.max(data, function (d) { return d.size + d.allocOffset; })]);
    //Memoriaterkep X tengelyenek beallitasa
    var memoryMapXaxis = d3.svg.axis()
        .scale(memoryMapXscale)
        .innerTickSize(-memoryMapHeight)
        .orient("bottom");
    //Memoriaterkep Y tengelyenek beallitasa
    var memoryMapYaxis = d3.svg.axis()
        .scale(memoryMapYscale)
        .innerTickSize(-memoryMapWidth)
        .orient("left");
    //Memoriaterkep Y tengelyenek kirajzolasa
    memoryMapSvg
        .append("g")
        .attr("class", "y axis memory")
        .attr("transform", "translate(" + margin.left + ", " + margin.top + ")")
        .call(memoryMapYaxis);
    //Memoriaterkep X tengelyenek kirajzolasa
    memoryMapSvg.append("g")
        .attr("class", "x axis memory")
        .attr("transform", "translate(" + margin.left + ", " + (height + margin.top) + ")")
        .call(memoryMapXaxis);
    //Memoriaterkep belso (teglalapokat tartalmazo) rajzteruletenek kirajzolasa
    var memoryMapContent = memoryMapSvg.append("g")
        .attr("class", "content")
        .attr("transform", "translate(" + margin.left + ", " + margin.top + ")");
    //Memoriaterkep kezdeti allapotat tartalmazo teglalapjainak kirajzolasa
    memoryMapContent.selectAll(".bar")
        .data(data) //adatkotes
        .enter().append("rect")
        .attr("class", "bar")
        .attr("x", function (datum) { return memoryMapXscale(datum.allocTime); }) //BalFelso sarok X=allokacios ido kezdete
        .attr("y", function (datum) { return memoryMapYscale(datum.allocOffset); }) //BalFelso sarok Y=mennyi memoria volt mar lefoglalva eddig
        .attr("fill", function (d, i, o) { return randomColors(i); }) //Veletlenszeru szin beallitasa
        .attr("height", function (datum) { return memoryMapYscale(datum.size); }) //Magassag=Allokacio merete
        .attr("width", function (datum) { return memoryMapXscale(datum.deallocTime - datum.allocTime); }); //Szelesseg=Allokacio idotartama
    //Zoom + gomb esemenykezelo hozzarendeles
    d3.select(".zoom-in").on("click", function (d, i, o) {
        zoomThreshold += 0.5;
        recalculateMemoryMap(zoomThreshold, data);
    });
    //Zoom - gomb esemenykezelo hozzarendeles
    d3.select(".zoom-out").on("click", function (d, i, o) {
        if (zoomThreshold >= 1.4) {
            zoomThreshold -= 0.5;
            recalculateMemoryMap(zoomThreshold, data);
        }
    });
});
//Zoomolas eseten a memoriaterkepet teljesen ujra kell inicializalni a zoom faktornak megfeleloen
function recalculateMemoryMap(threshold, result) {
    //Letutolso deallokacio idopontja (algoritmus futasanak vege)
    var maxAllocationTime = d3.max(result, function (d) { return d.deallocTime; });
    //Memoriaterkep virtualis rajzteruletenek meretbeallitasa az adatsor hosszanak fuggvenyeben (a tobb ideig tarto rendezeseket "szet kell huzni")
    var memoryMapWidth = (maxAllocationTime / 25 * threshold) - margin.left - margin.right, memoryMapHeight = (600 * threshold) - margin.top - margin.bottom;
    memoryMapSvg.attr("width", memoryMapWidth + margin.left + margin.right);
    memoryMapSvg.attr("height", memoryMapHeight + margin.top + margin.bottom);
    //Memoriaterkep X skalajanak beallitasa
    var memoryMapXscale = d3.scale.linear()
        .range([0, memoryMapWidth])
        .domain([0, maxAllocationTime]);
    //Memoriaterkep Y skalajanak beallitasa
    var memoryMapYscale = d3.scale.linear()
        .range([0, memoryMapHeight])
        .domain([0, d3.max(result, function (d) { return d.size + d.allocOffset; })]);
    //Memoriaterkep X tengelyenek beallitasa
    var memoryMapXaxis = d3.svg.axis()
        .scale(memoryMapXscale)
        .innerTickSize(-memoryMapHeight)
        .orient("bottom");
    //Memoriaterkep Y tengelyenek beallitasa
    var memoryMapYaxis = d3.svg.axis()
        .scale(memoryMapYscale)
        .innerTickSize(-memoryMapWidth)
        .orient("left");
    //Memoriaterkep X tengelyenek kirajzolasa
    memoryMapSvg.selectAll("g.x.axis.memory")
        .attr("transform", "translate(" + margin.left + ", " + (memoryMapHeight + margin.top) + ")")
        .call(memoryMapXaxis);
    //Memoriaterkep Y tengelyenek kirajzolasa
    memoryMapSvg.selectAll("g.y.axis.memory")
        .call(memoryMapYaxis);
    //Memoriaterkep tartalmanak ujrarajzolasa
    var memoryMapContent = memoryMapSvg.select("g.content");
    memoryMapContent.selectAll(".bar") //Nincs ujra adatkotes, mert az adat nem valtozott, csak a teglalapokat kell athelyezni, atmeretezni az uj zoom faktornak megeleloen
        .attr("x", function (datum) { return memoryMapXscale(datum.allocTime); })
        .attr("y", function (datum) { return memoryMapYscale(datum.allocOffset); })
        .attr("height", function (datum) { return memoryMapYscale(datum.size); })
        .attr("width", function (datum) { return memoryMapXscale(datum.deallocTime - datum.allocTime); });
}
