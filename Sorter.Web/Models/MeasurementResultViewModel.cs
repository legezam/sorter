﻿using Sorter.Algorithms;
using Sorter.Algorithms.Generators;
using Sorter.API.Measurement;
using Sorter.API.SequenceGenerators;
using System;

namespace Sorter.Web.Models
{
  public class MeasurementResultViewModel
  {
    public MeasurementSessionResult Result { get; set; }
    public string SequenceType { get; set; }
    public MeasurementType MeasurementType {get;set;}
    public int SequenceSize { get; set; }
    public SortType SortType { get; set; }
    public Guid SessionId { get; set; }
  }
}