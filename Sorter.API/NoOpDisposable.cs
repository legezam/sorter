﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sorter.API
{
  internal class Disposable : IDisposable
  {
    private Disposable() { }
    public void Dispose()
    {

    }

    private static readonly IDisposable instance = new Disposable();

    public static IDisposable Instance => instance;
  }
}
