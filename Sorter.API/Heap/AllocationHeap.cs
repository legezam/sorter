﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reactive;
using System.Reactive.Linq;

namespace Sorter.API.Heap
{
  internal class AllocationHeap : IHeap
  {
    private event EventHandler<DeallocationEventArgs> Deallocated;
    private readonly IObservable<DeallocationEventArgs> deallocatedStream;

    private long maxAllocationCount = 0;
    private long currentAllocation = 0;
    private readonly Stopwatch stopWatch;
    private readonly Stack<AllocationScope> stack;

    public AllocationHeap()
    {
      this.stopWatch = Stopwatch.StartNew();
      this.stack = new Stack<AllocationScope>(50);
      this.deallocatedStream = Observable.FromEventPattern<DeallocationEventArgs>(add => this.Deallocated += add, remove => this.Deallocated -= remove).Select(pattern => pattern.EventArgs);
    }

    public IObservable<DeallocationEventArgs> DeallocatedStream => this.deallocatedStream;

    public IDisposable Allocate(long numOfItems)
    {
      StackTrace stack = new StackTrace();
      this.currentAllocation += numOfItems;
      var newScope = new AllocationScope(this, numOfItems, this.stopWatch.ElapsedTicks, stack.FrameCount);
      this.stack.Push(newScope);
      this.maxAllocationCount = Math.Max(this.maxAllocationCount, this.currentAllocation);
      return newScope;
    }

    void IHeap.Deallocate(AllocationScope scope)
    {
      AllocationScope popped = this.stack.Pop();
      if (!object.ReferenceEquals(scope, popped))
      {
        throw new InvalidOperationException("Stack allocation size is not equal to scope allocation size");
      }

      this.currentAllocation -= popped.AllocationSize;
      
      if (this.Deallocated != null)
      {
        this.Deallocated(this, new DeallocationEventArgs(scope.Depth, scope.AllocationTime, 
                                      this.stopWatch.ElapsedTicks, scope.AllocationSize, this.currentAllocation));
      }
    }

    public long MaxAllocation => this.maxAllocationCount;

    public void Dispose()
    {
      this.stopWatch.Stop();
      if (this.stack.Any())
      {
        throw new InvalidOperationException("There are elements left in the stack");
      }
    }
  }
}
