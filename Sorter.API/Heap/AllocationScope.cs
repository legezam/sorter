﻿using System;

namespace Sorter.API.Heap
{
  public class AllocationScope : IDisposable
  {
    private readonly IHeap heap;
    private readonly long allocationSize;
    private readonly long allocationTime;
    private readonly int depth;

    internal AllocationScope(IHeap heap, long allocationSize, long allocationTime, int depth)
    {
      this.heap = heap;
      this.allocationSize = allocationSize;
      this.allocationTime = allocationTime;
      this.depth = depth;
    }

    public long AllocationSize => this.allocationSize;

    public long AllocationTime => this.allocationTime;

    public int Depth => this.depth;

    void IDisposable.Dispose()
    {
      this.heap.Deallocate(this);
    }
  }
}
