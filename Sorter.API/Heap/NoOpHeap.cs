﻿using System;
using System.Reactive.Linq;

namespace Sorter.API.Heap
{
  internal class NoOpHeap : IHeap
  {
    public IObservable<DeallocationEventArgs> DeallocatedStream => Observable.Empty<DeallocationEventArgs>();

    public long MaxAllocation => 0;

    public IDisposable Allocate(long numOfItems)
    {
      return Disposable.Instance;
    }

    public void Deallocate(AllocationScope scope) { }

    public void Dispose() { }

  }
}
