﻿using System;

namespace Sorter.API.Heap
{
  /// <summary>
  /// Interfész, amely memória allokáció/deallokáció funkcionalitást ír elő a leszármazottaknak.
  /// </summary>
  public interface IHeap : IDisposable
  {
    /// <summary>
    /// Observable Stream mely minden egyes deallokáció után kerül meghívásra.
    /// </summary>
    IObservable<DeallocationEventArgs> DeallocatedStream { get; }

    /// <summary>
    /// Lefoglalt memória felszabadítása.
    /// </summary>
    void Deallocate(AllocationScope scope);

    /// <summary>
    /// Memória lefoglalása
    /// </summary>
    IDisposable Allocate(long numOfItems);

    long MaxAllocation { get; }
  }
}
