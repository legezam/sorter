﻿using Newtonsoft.Json;
using Sorter.API.Serialization;
using System;
namespace Sorter.API.Heap
{
  public class DeallocationEventArgs : EventArgs, IJsonSerializable
  {
    public DeallocationEventArgs(int depth, long allocTime, long dealloctime, long size, long allocOffset)
    {
      this.DepthInStack = depth;
      this.AllocationTime = allocTime;
      this.DeallocationTime = dealloctime;
      this.Size = size;
      this.AllocationOffset = allocOffset;
    }

    public int DepthInStack { get; private set; }
    public long AllocationTime { get; private set; }
    public long DeallocationTime { get; private set; }
    public long Size { get; private set; }
    public long AllocationOffset { get; private set; }

    public void SerializeToJson(JsonTextWriter writer)
    {
      writer.WriteStartObject();
      writer.WritePropertyName("depth");
      writer.WriteValue(this.DepthInStack);
      writer.WritePropertyName("allocTime");
      writer.WriteValue(this.AllocationTime);
      writer.WritePropertyName("deallocTime");
      writer.WriteValue(this.DeallocationTime);
      writer.WritePropertyName("size");
      writer.WriteValue(this.Size);
      writer.WritePropertyName("allocOffset");
      writer.WriteValue(this.AllocationOffset);
      writer.WriteEndObject();
    }
  }
}
