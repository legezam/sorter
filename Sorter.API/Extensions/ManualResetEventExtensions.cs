﻿using System;
using System.Threading;

namespace Sorter.API.Extensions
{
  public static class ManualResetEventExtensions
  {
    public static void HoldEvent(this ManualResetEventSlim source, Action action)
    {
      try
      {
        source.Reset();
        action();
      }
      finally
      {
        source.Set();
      }
    }
  }
}
