﻿using Newtonsoft.Json;
using Sorter.API.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Sorter.API.TurnBased
{
  public class TurnCompletedEventArgs<T> : EventArgs, IJsonSerializable
  {
    private readonly int? turn;
    private readonly T[] state;
    public TurnCompletedEventArgs(int turn, IEnumerable<T> state)
    {
      this.turn = turn;
      this.state = state.ToArray();
    }

    public TurnCompletedEventArgs(IEnumerable<T> state)
    {
      this.state = state.ToArray();
    }

    public int? Turn => this.turn;

    public T[] State => this.state;

    public void SerializeToJson(JsonTextWriter writer)
    {
      writer.WriteStartObject();
      writer.WritePropertyName("turn");
      writer.WriteValue(this.Turn);
      writer.WritePropertyName("state");
      writer.WriteStartArray();
      foreach (T item in this.State)
      {
        writer.WriteValue(item);
      }
      writer.WriteEndArray();
      writer.WriteEndObject();
    }
  }
}
