﻿using System;

namespace Sorter.API.TurnBased
{
  public interface ITurnBased<T>
  {
    IObservable<TurnCompletedEventArgs<T>> TurnCompletedStream { get; }
  }
}
