﻿using System.Collections.Generic;

namespace Sorter.API.Measurement
{
  public class SortResult<T>
  {
    private readonly IEnumerable<T> result;
    private readonly PerformanceCounter perfCounter;

    public IEnumerable<T> Result => result;
    public PerformanceCounter PerfCounter => perfCounter;

    public SortResult(IEnumerable<T> result, PerformanceCounter perfCounter)
    {
      this.result = result;
      this.perfCounter = perfCounter;
    }
  }
}
