﻿namespace Sorter.API.Measurement
{
  public class PerformanceCounter
  {
    public int Comparisons { get; set; }
    public int Iterations { get; set; }
    public int MethodCalls { get; set; }
    public long ElapsedTicks { get; set; }
    public long MaxAllocation { get; set; }
  }
}
