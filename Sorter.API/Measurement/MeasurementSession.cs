﻿using Sorter.API.Heap;
using Sorter.API.SequenceGenerators;
using Sorter.API.Serialization;
using Sorter.API.Support;
using Sorter.API.TurnBased;
using System;
using System.IO;

namespace Sorter.API.Measurement
{
  public class MeasurementSession<T> :IDisposable
    where T : IComparable<T>
  {
    private const string DateTimeFormat = "yyyy_MM_dd_HH_mm_ss_fff";
    private const string FileNameFormat = "{0}_{1}.txt";
    private readonly MeasurementSetup<T> setup;
    private readonly Stream deallocStream;
    private readonly Stream turnCompletedStream;

    public MeasurementSession(MeasurementSetup<T> setup)
    {
      this.setup = setup;
      this.deallocStream = File.OpenWrite(Path.Combine(this.setup.OutputPath, GenerateDeallocationFileName()));
      this.turnCompletedStream = File.OpenWrite(Path.Combine(this.setup.OutputPath, GenerateTurnCompletedFileName()));
    }

    public MeasurementSessionResult StartSession()
    {
      SortResult<T> result = null;
      using (IHeap heap = setup.Measurement.GenerateHeap())
      {

        ISort<T> sorter = setup.Factory(heap);
        using (new ObservableStreamSerializer<DeallocationEventArgs>(heap.DeallocatedStream, this.deallocStream))
        {
          ITurnBased<T> turnBased = sorter as ITurnBased<T>;

          using ((this.setup.Measurement == MeasurementType.Performance || turnBased == null ) ? Disposable.Instance : 
            new ObservableStreamSerializer<TurnCompletedEventArgs<T>>(turnBased.TurnCompletedStream, this.turnCompletedStream) as IDisposable)
          {
            var sequence = this.setup.SequenceType.GetSequenceGenerator().GenerateSequence(setup.SequenceSize);
            result = sorter.Sort(sequence);
          }    
        }
      }

      return new MeasurementSessionResult(GenerateDeallocationFileName(), GenerateTurnCompletedFileName(), result.PerfCounter);
    }

    private string GenerateDeallocationFileName()
    {
      return string.Format(FileNameFormat, this.setup.SessionTime.ToString(DateTimeFormat), "Alloc");
    }

    private string GenerateTurnCompletedFileName()
    {
      return string.Format(FileNameFormat, this.setup.SessionTime.ToString(DateTimeFormat), "Turns");
    }

    public void Dispose()
    {
      this.turnCompletedStream.Flush();
      this.turnCompletedStream.Close();
      this.turnCompletedStream.Dispose();
      this.deallocStream.Flush();
      this.deallocStream.Close();
      this.deallocStream.Dispose();
    }
  }
}
