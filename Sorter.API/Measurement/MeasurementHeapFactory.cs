﻿using Sorter.API.Heap;
using System;

namespace Sorter.API.Measurement
{
  internal static class MeasurementHeapFactory
  {
    public static IHeap GenerateHeap(this MeasurementType type)
    {
      switch (type)
      {
        case MeasurementType.AllocationAndTurns:
          return new AllocationHeap();
        case MeasurementType.Performance:
          return new NoOpHeap();
        default:
          throw new ArgumentOutOfRangeException("type");
      }
    }
  }
}
