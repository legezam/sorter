﻿namespace Sorter.API.Measurement
{
  public class MeasurementSessionResult
  {
    public string DeallocLog
    {
      get; set;
    }
    public string TurnCompletedLog
    {
      get; set;
    }
    public PerformanceCounter PerformanceCounter
    {
      get; set;
    }
    public MeasurementSessionResult(string deallocLog, string turnCompletedLog, PerformanceCounter perfCount)
    {
      this.DeallocLog = deallocLog;
      this.TurnCompletedLog = turnCompletedLog;
      this.PerformanceCounter = perfCount;
    }
    public MeasurementSessionResult()
    {

    }
  }
}
