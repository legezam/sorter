﻿using Sorter.API.Heap;
using System;

namespace Sorter.API.Measurement
{
  public enum MeasurementType
  {
    AllocationAndTurns,
    Performance
  }

}
