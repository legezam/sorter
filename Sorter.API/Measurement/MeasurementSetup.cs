﻿using Sorter.API.Heap;
using Sorter.API.SequenceGenerators;
using Sorter.API.Support;
using System;

namespace Sorter.API.Measurement
{
  public class MeasurementSetup<T>
    where T : IComparable<T>
  {
    Func<IHeap, ISort<T>> factory;
    MeasurementType measurement;
    SequenceProvider<T> sequenceType;
    int sequenceSize;
    DateTime sessionTime;
    string outputPath;

    private MeasurementSetup()
    {
    }
    public Func<IHeap, ISort<T>> Factory => factory;
    public MeasurementType Measurement => measurement;
    public SequenceProvider<T> SequenceType => sequenceType;
    public int SequenceSize => sequenceSize;
    public DateTime SessionTime => sessionTime;
    public string OutputPath => outputPath;

    public class Builder
    {
      public Builder()
      {

      }

      public Builder(MeasurementSetup<T> basedOn)
      {
        this.Factory = basedOn.Factory;
        this.Measurement = basedOn.Measurement;
        this.SequenceType = basedOn.SequenceType;
        this.SequenceSize = basedOn.SequenceSize;
        this.SessionTime = basedOn.SessionTime;
        this.OutputPath = basedOn.OutputPath;
      }

      public Func<IHeap, ISort<T>> Factory { get; set; }
      public MeasurementType Measurement { get; set; }
      public SequenceProvider<T> SequenceType { get; set; }
      public int SequenceSize { get; set; }
      public DateTime SessionTime { get; set; }
      public string OutputPath { get; set; }

      public MeasurementSetup<T> Build()
      {
        MeasurementSetup<T> setup = new MeasurementSetup<T>();
        setup.factory = this.Factory;
        setup.measurement = this.Measurement;
        setup.sequenceType = this.SequenceType;
        setup.sequenceSize = this.SequenceSize;
        setup.sessionTime = this.SessionTime;
        setup.outputPath = this.OutputPath;
        return setup;
      }

      public static implicit operator MeasurementSetup<T>(Builder builder)
      {
        return builder.Build();
      }
    }
  }
  

}
