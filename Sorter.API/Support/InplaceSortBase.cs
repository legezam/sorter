﻿using Sorter.API;
using Sorter.API.Heap;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Sorter.API.Support
{
  public abstract class InplaceSortBase<T> : SortBase<T>
    where T : IComparable<T>
  {
    protected InplaceSortBase(IHeap heap) : base(heap) { }

    protected override IEnumerable<T> SortImpl(IEnumerable<T> source)
    {
      T[] tmp = source.ToArray();
      using (var scope = this.heap.Allocate(tmp.LongLength))
      {
        return SortInplace(tmp);
      }
    }

    protected abstract T[] SortInplace(T[] source);
  }
}
