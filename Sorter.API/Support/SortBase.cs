﻿using Sorter.API.Heap;
using Sorter.API.Measurement;
using Sorter.API.TurnBased;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reactive.Linq;

namespace Sorter.API.Support
{
  public abstract class SortBase<T> : ITurnBased<T>, ISort<T>
    where T : IComparable<T>
  {
    protected readonly IHeap heap;
    protected readonly Measurement.PerformanceCounter perfCounter;
    protected Stopwatch stopwatch;
    private event EventHandler<TurnCompletedEventArgs<T>> TurnCompleted;
    private readonly IObservable<TurnCompletedEventArgs<T>> turnCompletedStream;

    protected SortBase(IHeap heap)
    {
      this.turnCompletedStream = Observable.FromEventPattern<TurnCompletedEventArgs<T>>(
        add => TurnCompleted += add, remove => TurnCompleted -= remove).Select(evenpattern => evenpattern.EventArgs);
      this.heap = heap;
      this.perfCounter = new Measurement.PerformanceCounter();
    }

    public IObservable<TurnCompletedEventArgs<T>> TurnCompletedStream => this.turnCompletedStream
      .Select((eventpattern, index) => !eventpattern.Turn.HasValue ? new TurnCompletedEventArgs<T>(index, eventpattern.State) : eventpattern);

    public abstract string Name { get; }

    protected void RaiseTurnCompleted(T[] source, int turn)
    {
      if (TurnCompleted != null)
      {
        TurnCompleted(this, new TurnCompletedEventArgs<T>(turn, source));
      }
    }

    protected void RaiseTurnCompleted(T[] source)
    {
      if (TurnCompleted != null)
      {
        TurnCompleted(this, new TurnCompletedEventArgs<T>(source));
      }
    }

    protected void Swap(int[] array, long i, long j)
    {
      this.perfCounter.MethodCalls++;
      using (this.heap.Allocate(1))
      {
        int tmp = array[i];
        array[i] = array[j];
        array[j] = tmp;
      }
    }

    public SortResult<T> Sort(IEnumerable<T> source)
    {
      stopwatch = Stopwatch.StartNew();
      var result = SortImpl(source);
      stopwatch.Stop();
      this.perfCounter.ElapsedTicks = stopwatch.ElapsedTicks;
      this.perfCounter.MaxAllocation = heap.MaxAllocation;
      return new SortResult<T>(result, this.perfCounter);
    }

    protected abstract IEnumerable<T> SortImpl(IEnumerable<T> source);
  }
}
