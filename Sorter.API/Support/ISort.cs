﻿using Sorter.API.Measurement;
using Sorter.API.TurnBased;
using System;
using System.Collections.Generic;

namespace Sorter.API.Support
{
  public interface ISort<T>
    where T : IComparable<T>
  {
    SortResult<T> Sort(IEnumerable<T> source);

    string Name { get; }
  }
}
