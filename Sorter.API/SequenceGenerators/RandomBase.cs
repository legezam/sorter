﻿using System;
using System.Security.Cryptography;

namespace Sorter.API.SequenceGenerators
{
  public abstract class RandomBase
  {
    protected static Random GenerateRandomGenerator()
    {
      byte[] randomBuffer = new byte[4];
      RandomNumberGenerator.Create().GetBytes(randomBuffer);
      int seed = BitConverter.ToInt32(randomBuffer, 0);
      return new Random(seed);
    }
  }
}
