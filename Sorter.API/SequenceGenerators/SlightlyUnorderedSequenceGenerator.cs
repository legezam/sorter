﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sorter.API.SequenceGenerators
{
  class SlightlyUnorderedSequenceGenerator : RandomBase, ISequenceGenerator<int>
  {
    public IEnumerable<int> GenerateSequence(int numOfElements)
    {
      int[] array = Enumerable.Range(0, numOfElements).ToArray();
      Randomize(array);
      return array;
    }

    private void Randomize(int[] array)
    {
      int tenPercent = (int)(array.Length * 0.1);
      Random random = GenerateRandomGenerator();
      for (int i = 0; i < tenPercent; i++)
      {
        Swap(array, random.Next(0, array.Length), random.Next(0, array.Length));
      }
    }

    private void Swap(int[] source, int a, int b)
    {
      int tmp = source[a];
      source[a] = source[b];
      source[b] = tmp;
    }
  }
}
