﻿using System.Collections.Generic;

namespace Sorter.API.SequenceGenerators
{
  public interface ISequenceGenerator<T>
  {
    IEnumerable<T> GenerateSequence(int numOfElements);
  }
}
