﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Sorter.API.SequenceGenerators
{
  internal class RandomSequenceGenerator : RandomBase, ISequenceGenerator<int>
  {
    public IEnumerable<int> GenerateSequence(int numOfElements)
    {
      Random rand = GenerateRandomGenerator();
      return Enumerable.Range(0, numOfElements).OrderBy(item => rand.Next());
    }

    
  }
}
