﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sorter.API.SequenceGenerators
{
  internal class FullyReverseOrderedSequenceGenerator : ISequenceGenerator<int>
  {
    public IEnumerable<int> GenerateSequence(int numOfElements)
    {
      return Enumerable.Range(0, numOfElements).OrderByDescending(item => item);
    }
  }
}
