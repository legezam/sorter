﻿using System;
using System.Collections.Generic;

namespace Sorter.API.SequenceGenerators
{
  public class SequenceProvider<T>
  {
    private readonly string name;
    private readonly Func<ISequenceGenerator<T>> getSequenceGenerator;

    public string Name => this.name;
    public Func<ISequenceGenerator<T>> GetSequenceGenerator => this.getSequenceGenerator;

    protected SequenceProvider(Func<ISequenceGenerator<T>> getGenerator, string name)
    {
      this.name = name;
      this.getSequenceGenerator = getGenerator;
    }

    public override string ToString()
    {
      return this.name;
    }
    #region [Predefined Integer Generators]
    private const string FullyOrderedName = "FullyOrdered";
    private const string FullyReverseOrderedName = "FullyReverseOrdered";
    private const string RandomName = "Random";
    private const string SlightlyUnOrderedName = "SlightlyUnordered";

    public static readonly SequenceProvider<int> FullyOrdered = new SequenceProvider<int>(() => new FullyOrderedSequenceGenerator(), FullyOrderedName);
    public static readonly SequenceProvider<int> FullyReverseOrdered = new SequenceProvider<int>(() => new FullyReverseOrderedSequenceGenerator(), FullyReverseOrderedName);
    public static readonly SequenceProvider<int> Random = new SequenceProvider<int>(() => new RandomSequenceGenerator(), RandomName);
    public static readonly SequenceProvider<int> SlightlyUnOrdered = new SequenceProvider<int>(() => new SlightlyUnorderedSequenceGenerator(), SlightlyUnOrderedName);


    public static IEnumerable<SequenceProvider<int>> AllIntegerGenerators
    {
      get
      {
        yield return Random;
        yield return FullyOrdered;
        yield return FullyReverseOrdered;
        yield return SlightlyUnOrdered;
      }
    }

    public static SequenceProvider<int> ParseIntegerGenerator(string name)
    {
      switch (name)
      {
        case FullyOrderedName:
          return FullyOrdered;
        case FullyReverseOrderedName:
          return FullyReverseOrdered;
        case RandomName:
          return Random;
        case SlightlyUnOrderedName:
          return SlightlyUnOrdered;
        default:
          throw new ArgumentOutOfRangeException(nameof(name));
      }
    }
    #endregion [Predefined Integer Generators]
  }
}
