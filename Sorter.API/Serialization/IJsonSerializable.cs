﻿using Newtonsoft.Json;

namespace Sorter.API.Serialization
{
  internal interface IJsonSerializable
  {
    void SerializeToJson(JsonTextWriter writer);
  }
}
