﻿using Newtonsoft.Json;
using Sorter.API.Extensions;
using System;
using System.Collections.Concurrent;
using System.IO;
using System.Linq;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Sorter.API.Serialization
{
  /// <summary>
  /// Egy Observable kollekció Stream-be serializálását végző osztály.
  /// </summary>
  /// <typeparam name="T">Bármilyen JsonSerializable interfészt megvalósító objektum</typeparam>
  internal class ObservableStreamSerializer<T> : IDisposable
    where T : IJsonSerializable
  {
    private readonly CancellationTokenSource cancelOperation;
    private readonly IDisposable sourceStream;
    private readonly Stream fileStream;
    private readonly BlockingCollection<T> collection;
    private readonly ManualResetEventSlim writerEvent;

    protected readonly JsonTextWriter writer;

    public ObservableStreamSerializer(IObservable<T> sourceStream, Stream file)
    {
      this.collection =
        new BlockingCollection<T>(
          new ConcurrentQueue<T>());

      this.cancelOperation = new CancellationTokenSource();
      this.writerEvent = new ManualResetEventSlim(true);
      this.fileStream = file;

      this.writer = new JsonTextWriter(new StreamWriter(fileStream));
      this.StartStream();

      this.sourceStream = sourceStream.Subscribe(items => AdditemToQueue(items));

      Task.Run(() => WriterQueue());
    }

    private void AdditemToQueue(T items)
    {
      this.collection.Add(items);
    }

    private void WriterQueue()
    {
      try
      {
        while (!cancelOperation.Token.IsCancellationRequested)
        {
          writerEvent.HoldEvent(() => {
            T item = collection.Take(cancelOperation.Token);
            item.SerializeToJson(writer);

            while (collection.Any())
            {
              item = collection.Take();
              item.SerializeToJson(writer);
            }
          });
        }
      }
      catch (OperationCanceledException)
      {
      }
    }
    private void StartStream()
    {
      this.writer.Formatting = Formatting.Indented;
      this.writer.Indentation = 2;
      this.writer.IndentChar = ' ';
      this.writer.WriteStartArray();
    }

    private void CloseStream()
    {
      writer.WriteEndArray();
      writer.Flush();
    }

    public void Dispose()
    {
      this.sourceStream.Dispose();
      cancelOperation.Cancel();

      if (!this.writerEvent.IsSet)
      {
        this.writerEvent.Wait();
      }

      if (collection.Any())
      {
        while (collection.Any())
        {
          T item = collection.Take();
          item.SerializeToJson(writer);
        }
      }
      this.CloseStream();

    }
  }
}
