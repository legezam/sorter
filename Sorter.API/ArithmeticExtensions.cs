﻿namespace Sorter.API
{
  public static class ArithmeticExtensions
  {
    public static int IncrementIf(this int source, bool condition)
    {
      return condition ? ++source : source;
    }

    public static long IncrementIf(this long source, bool condition)
    {
      return condition ? ++source : source;
    }
  }
}
